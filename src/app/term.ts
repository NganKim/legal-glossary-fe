export interface Term {
	id: number;
	english: string,
	vietnamese: string,
	note: string,
	relatedTerms: string,
	created: string,
    lastUpdated: string,
    createdBy: string,
    lastUpdatedBy: string;
}