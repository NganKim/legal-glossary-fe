import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Term } from '../term'
import { TermService } from '../term.service'

@Component({
  selector: 'app-term-detail',
  templateUrl: './term-detail.component.html',
  styleUrls: ['./term-detail.component.scss']
})
export class TermDetailComponent implements OnInit {
  @Input() term?: Term;

  constructor(
    private termService: TermService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getTerm();
  }

  getTerm(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.termService.getTerm(id)
      .subscribe(term => this.term = term);
    console.log('id:' + id);
    console.log('term: ' + this.term?.english);
  }
}
