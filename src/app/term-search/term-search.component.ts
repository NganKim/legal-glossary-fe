import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Term } from '../term';
import { TermService } from '../term.service';

@Component({
  selector: 'app-term-search',
  templateUrl: './term-search.component.html',
  styleUrls: ['./term-search.component.scss']
})
export class TermSearchComponent implements OnInit {
  searchKey = new Subject<string>();
  searchResults$!: Observable<Term[]>;

  constructor(private termService: TermService) { }

  ngOnInit(): void {
    this.searchResults$ = this.searchKey.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term : string) => this.termService.searchTerms(term)),
    );
  }

  search(key: string) : void {
    this.searchKey.next(key);
  }

}
