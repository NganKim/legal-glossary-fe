import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TermDetailComponent } from './term-detail/term-detail.component';
import { TermSearchComponent } from './term-search/term-search.component';

const routes: Routes = [
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  { path: 'search', component: TermSearchComponent },
  { path: 'terms/:id', component: TermDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
