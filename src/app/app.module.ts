import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TermDetailComponent } from './term-detail/term-detail.component';
import { TipsComponent } from './tips/tips.component';
import { TermSearchComponent } from './term-search/term-search.component';

@NgModule({
  declarations: [
    AppComponent,
    TermDetailComponent,
    TipsComponent,
    TermSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
