import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Term } from './term';

@Injectable({
  providedIn: 'root'
})
export class TermService {
  private serverUrl: string = 'http://localhost:8080/';
  private termsUrl: string = this.serverUrl + 'api/terms';
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  searchTerms(key: string): Observable<Term[]> {
    const url = this.termsUrl + "/search";
    if (!key.trim()) {
      return of([]);
    }
    return this.http.get<Term[]>(`${url}/${key}`);
  }

  getTerm(id: number): Observable<Term> {
    const url = `${this.termsUrl}/${id}`;
    return this.http.get<Term>(url)
      .pipe(
        tap(_ => this.log(`fetched term id=${id}`)),
        catchError(this.handleError<Term>(`getTerm id=${id}`))
      );
  } 

  getTerms(): Observable<Term[]> {
    return this.http.get<Term[]>(this.termsUrl)
      .pipe(
        tap(_ => this.log('fetched terms')),
        catchError(this.handleError<Term[]>('getTerms', []))
      );
  }

   private log(message: string) {
     console.log(`TermService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
